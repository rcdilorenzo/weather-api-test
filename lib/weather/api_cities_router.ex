defmodule ApiCitiesRouter do
  use Dynamo.Router

  get "/" do
    # render conn, "index.html"
    conn.resp 200, [cities: Repository.cities_to_json] |> ExJSON.generate
  end

  put "/:city_id" do
    render_json Repository.get(City, conn.params[:city_id]), conn, fn (city) ->
      city = city.update(conn.params |> Dict.to_list)
      Repository.update(city)
      city
    end
  end

  post "/" do
    render_json Repository.create(City.new(name: conn.params[:name])), conn, fn (city) ->
      city.update(conn.params |> Dict.to_list)
      Repository.update(city)
      city
    end
  end

  delete "/:city_id" do
    try do
      city = Repository.get(City, conn.params[:city_id]) |> Repository.delete
      conn.resp 200, [message: "Delete successful."] |> ExJSON.generate
    rescue
      x -> conn.resp 500, [error: x.message] |> ExJSON.generate
    end
  end

  def render_json(city, conn, func) do
    try do
      case City.validate city do
        [] ->
          conn.resp 200, func.(city) |> City.to_json |> ExJSON.generate
        errors ->
          conn.resp 400, [errors: errors] |> ExJSON.generate
      end
    rescue
      x -> conn.resp 500, [error: x.message] |> ExJSON.generate
    end
  end

end