defmodule CitiesRouter do
  use Dynamo
  use Dynamo.Router

  config :dynamo, templates_paths: [Path.expand("../../web/templates/layouts", __DIR__)]

  prepare do
    conn.assign(:layout, "main")
  end

  get "/" do
    conn = conn.assign(:title, "Hello World!")
    conn = conn.assign(:cities, Repository.cities)
    render conn, "index.html"
  end

end