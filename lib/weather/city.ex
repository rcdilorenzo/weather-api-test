defmodule City do
  use Ecto.Model

  def to_json(city), do: [id: city.id, name: city.name, temp_f: city.temp_f]

  validate user, name: present()

  queryable "cities" do
    field :name, :string
    field :temp_f, :string
  end
end