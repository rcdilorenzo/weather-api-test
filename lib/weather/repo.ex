defmodule Repository do
  use Ecto.Repo, adapter: Ecto.Adapters.Postgres, env: Mix.env
  import Ecto.Query

  def url(:dev), do: "ecto://postgres:postgres@localhost/weather"
  def url(:prod), do: "ecto://yljarevxtspwce:yhX6_Zw0Me1GNL50INw9W2Xi6a@ec2-54-197-238-239.compute-1.amazonaws.com:5432/dfsf9hjslb70lc"
  def priv, do: app_dir(:weather, "priv/repo")

  def cities_to_json do
    lc city inlist cities, do: city |> City.to_json
  end

  def cities do
    all(from c in City, order_by: c.id, select: c)
  end
end