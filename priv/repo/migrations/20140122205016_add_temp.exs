defmodule Repository.Migrations.AddTemp do
  use Ecto.Migration

  def up do
    "ALTER TABLE cities ADD temp_f varchar(50) NULL DEFAULT '80.0'"
  end

  def down do
    "ALTER TABLE cities DROP temp_f"
  end
end
