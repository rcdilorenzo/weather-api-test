defmodule Weather.Repo.Migrations.CreateCities do
  use Ecto.Migration

  def up do
    "CREATE TABLE cities(id SERIAL, name varchar(40))"
  end

  def down do
    "DROP TABLE cities"
  end
end
