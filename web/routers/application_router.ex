defmodule ApplicationRouter do
  use Dynamo.Router
  import Dynamo.HTTP.Redirect

  prepare do
    conn.fetch([:cookies, :params])
  end

  forward "/api/cities", to: ApiCitiesRouter
  forward "/cities", to: CitiesRouter

  get "/" do
    redirect conn, to: "/cities"
  end

end
