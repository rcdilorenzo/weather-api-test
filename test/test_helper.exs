Dynamo.under_test(Weather.Dynamo)
Dynamo.Loader.enable
ExUnit.start

defmodule Weather.TestCase do
  use ExUnit.CaseTemplate

  # Enable code reloading on test cases
  setup do
    Dynamo.Loader.enable
    :ok
  end
end
